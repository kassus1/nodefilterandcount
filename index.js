#!/usr/bin/env node

//Declare constante for calling methods and data

const chalk = require('chalk');
const clear = require('clear');
const figlet  = require('figlet');
const inquirer = require("inquirer");

// get all data
const data = require("./data");

// get filetred people
const filteredPeople = [];

/**
 * here is the implement of  the start-up phase of our console application.
 */

/**
 *   This is use to clear the screen and then display a banner
 */

 // clear method
clear();

// This is use to display a banner
console.log(
  chalk.yellow(
    figlet.textSync('filterandcount', { horizontalLayout: 'full' })
  )
);

/**
 * This use to get the filtered data for one people object
 */

filteredArray = (dataArray) => {
  // Get the entry data in params
  const dataTofilter = dataArray;
  dataTofilter.forEach(eltPeople => {
    const people = eltPeople.people;
        // get all people in one array
        for (let i = 0; i < people.length; i++) {
          const peopleItem = people[i];
          const peopleAnimalsItem = [];
          peopleAnimalsItem.push(peopleItem.animals);
            // get animals array
            peopleAnimalsItem.forEach(eltAnimal => {
              const animal = eltAnimal;
              for (let j = 0; j < animal.length; j++) {
                const animalItem = animal[j];
                // get one animal name
                const animalName = animalItem.name;
                // constante width 'ry' value for filtering all animal name
                const ourSubstring = "ry";
                /**
                 *  filter all animal name 
                 * If animalName.includes return false , we delete the animal array concern
                 * 
                 *  */
              
                if (animalName.indexOf(ourSubstring) !== -1) {
                  console.log("The word ry is in the animal string.");
                } else {
                  animal.splice(j, 1);
                  // create people count array
                  peopleItem.countPeople = [];
                  // create animal count array
                  peopleItem.countAnimal = [];
                  // Store the people count number
                  peopleItem.countPeople.push(people.length);
                   // Store the animal count number
                  peopleItem.countAnimal.push(peopleItem.animals.length);
                  filteredPeople.push(people);
                }
             
              }
            })
        };
      
  })
   console.log('display filtered result', JSON.stringify(filteredPeople[4]));
}

/**
 * This use to get the filtered data for one people object
 */
printCounts = (dataArray) => {
  // Get the entry data in params
  const dataToPrints = dataArray;
  dataToPrints.forEach(eltPeople => {
    const people = eltPeople.people;
        // get all people in one array
        for (let i = 0; i < people.length; i++) {
          const peopleItem = people[i];
          const peopleAnimalsItem = [];
          peopleAnimalsItem.push(peopleItem.animals);
            // get animals array
            peopleAnimalsItem.forEach(eltAnimal => {
              const animal = eltAnimal;
              for (let j = 0; j < animal.length; j++) {
                const animalItem = animal[j];
                // get one animal name
                const animalName = animalItem.name;
                  // create people count array
                  peopleItem.countPeople = [];
                  // create animal count array
                  peopleItem.countAnimal = [];
                  // Store the people count number
                  peopleItem.countPeople.push(people.length);
                   // Store the animal count number
                  peopleItem.countAnimal.push(peopleItem.animals.length);
                  filteredPeople.push(people);
              }
            })
        };
  })
  console.log('Display counts numbers', JSON.stringify(filteredPeople));
}

inquirer
  .prompt([
    {
      type: "list",
      name: "action",
      message: "Match Actions?",
      choices: ["filter"]
    }
  ])
  .then(answer => {
    // display filtered Array
    const a = filteredArray(data.data);
    inquirer
      .prompt([
        {
          type: "list",
          name: "action",
          message: "Match Actions?",
          choices: ["printsCounts"]
  
        }
      ])
      .then(answers => {
        // display array counts elements
        const b = printCounts(data.data);
      }
      )
  });
